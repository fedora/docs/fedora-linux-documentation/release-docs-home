= Upgrading Your Current System
The Fedora docs team
:revnumber: F39
:revdate: 2023-11-27 

This chapter explains how to upgrade your existing Fedora installation to the current release.
There are two basic ways to do so:

Automatic upgrade using [application]*dnf system upgrade*::
The preferred way to upgrade your system is an automatic upgrade using the [application]*dnf system upgrade* utility.
For information on performing an automatic upgrade, see link: xref:quick-docs:ROOT:upgrading-fedora-offline.adoc[Upgrading Fedora using the DNF system upgrade plugin].

Manual Reinstallation::
Instead of relying on `dnf system upgrade`, you can _reinstall_ the latest version of Fedora.
This involves booting the installer as if you were performing a clean installation, letting it detect your existing Fedora system, and overwriting the root partition while preserving data on other partitions and volumes.
The same process can also be used to reinstall the system, if you need to do so for any reason.
For detailed information, see _Manual System Upgrade or Reinstallation_.

//xref:sect-upgrading-fedora-manual-reinstall[Manual System Upgrade or Reinstallation].

[IMPORTANT]
====

Always back up your data before performing an upgrade or reinstalling your system, no matter which method you choose.

====

[[sect-upgrading-fedora-manual-reinstall]]
== Manual System Upgrade or Reinstallation

Unfortunately, we have not written this chapter yet, and there is no dedicated documentation about a manual reinstall on the Wiki, either.
In the meantime before we fix this, you can try to start the installation normally (from a boot CD/DVD/USB), select manual partitioning in your installer, and reuse existing partitions instead of destroying them and creating new ones.
The instructions at _Manual Partitioning_ should in most cases be easy to adapt for this.

//xref:install/Installing_Using_Anaconda.adoc#sect-installation-gui-manual-partitioning[Manual Partitioning] should in most cases be easy to adapt for this.
